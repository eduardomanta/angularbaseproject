import { Injectable } from '@angular/core';
import { HOST_BACKEND } from '../shared/constants';
import { HttpClient } from '@angular/common/http';
import { Encuesta } from '../models/encuesta';
import { Respuesta } from '../models/respuesta';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EncuestaService {
  private urlSave:string=`${HOST_BACKEND}/api/encuesta/save`;
  private urlList:string=`${HOST_BACKEND}/api/encuesta/list`;
  constructor(private http:HttpClient) { }
  save(encuesta:Encuesta){
    return this.http.post<Respuesta>(this.urlSave,encuesta);
  }
  list():Observable<any>{
    return this.http.get<Array<Encuesta>>(this.urlList,{});
  }
}
