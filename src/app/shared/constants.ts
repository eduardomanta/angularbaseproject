export const HOST_BACKEND = 'http://localhost:8080';
export const TOKEN_NAME = "idToken";
export const REFRESH_TOKEN_NAME = "refreshToken";
export const ACCESS_TOKEN_NAME = "accessToken";
export const PARAM_USUARIO = "usuario";
export const SESION_NAME = "sessionId";