import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Respuesta } from 'src/app/models/respuesta';
import { LoginDTO } from 'src/app/dto/login-dto';
import { PARAM_USUARIO, TOKEN_NAME, SESION_NAME } from 'src/app/shared/constants';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private loginDTO:LoginDTO;

  constructor(private router:Router, private authentication:AuthenticationService) {
    this.loginDTO= new LoginDTO();
   }
  ngOnInit() {

  }
  onLogin(){
    this.authentication.login(this.loginDTO).subscribe((data:Respuesta)=>{
        if(data.status==="OK"){
          localStorage.setItem(TOKEN_NAME,data.accessToken);
          localStorage.setItem(PARAM_USUARIO,JSON.stringify(data.body));
          this.authentication.setObservableUser(data.body);
          if(this.authentication.isRoleAdmin()){
            this.router.navigate(["admin/encuesta-lista"]);
          }else{
            this.router.navigate(["encuesta"]);
          }
        }else if(data.status==='OK-UPDATE'){
          localStorage.setItem(SESION_NAME,data.sessionId);
          this.router.navigate(["login-first-reset"],{ queryParams: { username: this.loginDTO.username } });
        }
    },(err)=>{
      localStorage.removeItem(TOKEN_NAME);
      console.log(err);
    });
  }
}
