import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Encuesta } from 'src/app/models/encuesta';
import { EncuestaService } from 'src/app/services/encuesta.service';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styleUrls: ['./encuesta.component.css']
})
export class EncuestaComponent implements OnInit {
  public registerForm: FormGroup;
  private encuesta:Encuesta;
  constructor(private encuestaService:EncuestaService,private formBuilder: FormBuilder) { 
    this.encuesta=new Encuesta();
  }
  get f() { return this.registerForm.controls; }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
        nombres: ['', Validators.required],
        apellidoPaterno: ['', Validators.required],
        apellidoMaterno: ['', Validators.required],
        edad: ['', [Validators.required,  Validators.pattern("^[0-9]*$")]],
        curso: ['', [Validators.required]],
    });
  }

  onSaveEncuesta(){
    if(!this.registerForm.valid){
      return;
    }
    this.encuestaService.save(this.encuesta).subscribe((data)=>{
      if(data.status==="OK"){
        this.encuesta=new Encuesta();
      }
    },(err)=>{
      console.log(err);
    });
  }
}
