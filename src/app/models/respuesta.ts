export class Respuesta {
    status:string;
	accessToken:string;
	idToken:string;
	refreshToken:string;
	sessionId:string;
	body:object;
}
