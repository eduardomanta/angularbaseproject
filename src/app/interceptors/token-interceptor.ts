import { HttpInterceptor, HttpRequest, HttpHandler, HttpResponse, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TOKEN_NAME, PARAM_USUARIO } from '../shared/constants';
import { AuthenticationService } from '../services/authentication.service';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
@Injectable({
    providedIn:'root'
})
export class TokenInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService,private router:Router) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      let token = localStorage.getItem(TOKEN_NAME);
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${token}`
        }
      });
      return next.handle(request).pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
            //no hacemos nada porque todo está ok
          }
        }, err => {
          console.log("err: "+JSON.stringify(err));
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401 || err.status === 403) {
              alert("Ocurrió un error al validar su token... intente loguearse nuevamente");
              localStorage.removeItem(PARAM_USUARIO);
              localStorage.removeItem(TOKEN_NAME);
              this.router.navigate(['login']);
              //this.authenticationService.cerrarSesion();
            }
          }
        })
      );
    }
}
