import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouteRoutingModule } from './route-routing.module';
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouteRoutingModule
  ],
  exports:[
    RouteRoutingModule
  ]
})
export class RouteModule { }
