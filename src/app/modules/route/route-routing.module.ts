import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EncuestaComponent } from '../../components/encuesta/encuesta.component';
import { HomeComponent } from '../../components/home/home.component';
import { AuthGuard } from '../../guards/auth-guard';
import { LoginComponent } from 'src/app/components/login/login.component';
import { SecurityComponent } from 'src/app/components/security/security.component';
import { EncuestaListaComponent } from 'src/app/components/encuesta-lista/encuesta-lista.component';
import { LoginFirstPasswordComponent } from 'src/app/components/login-first-password/login-first-password.component';
const routes: Routes = [  
  {
    path: '',
    component: HomeComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'security',
    component: SecurityComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'login-first-reset',
    component: LoginFirstPasswordComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'encuesta',
    component: EncuestaComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'admin/encuesta-lista',
    component: EncuestaListaComponent,
    canActivate:[AuthGuard]
  },
  { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RouteRoutingModule { }
