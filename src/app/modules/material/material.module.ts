import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MatButtonModule, MatCheckboxModule,MatInputModule,MatIconModule,MatMenuModule,MatRadioModule, MatTableModule} from '@angular/material';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatRadioModule,
    MatTableModule,
    FormsModule
  ],
  exports:[
    MatButtonModule, 
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatMenuModule,
    MatRadioModule,
    MatTableModule,
    FormsModule
  ]
})
export class MaterialModule { }
